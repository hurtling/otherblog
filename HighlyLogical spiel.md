(An idea for a minecraft mod from 2011 -- an entertaining introduction)

I am a lazy developer, and not all of the work here is probably mine (Although if it's not, it's because that work is GPL, or similar).
However, it shouldn't have taken a person like me to make a no-frills redstone logic-gates mod for bukkit.

The idea behind this is simple:
HighlyLogical won't try to provide an entirely new game mechanic teetering on top of a very precarious tower of barely compatible code. (Java VM, JLWGL, MC by Notch, CraftBukkit, your other mods... anyone else got vertigo?)
HighlyLogical won't ever contain a virtual computer, eating up already scarce REAL computer cycles.
HighlyLogical won't take up 600 extra item IDs, crushing your dreams of compatibility with your other favourite mods under its exquisitely booted heel.
HighlyLogical won't take 6 months and 100 new features to JUST TO RELEASE AN UPDATE COMPATIBLE WITH THE LATEST CLIENT AND SERVER.

The wheel has already been well and truly invented.
Just ask the 2 endgame bosses I'm sure have both been released by the time you read this.
Just ask the 3rd lighting system for the game to date, and the 3rd world map format.

I'm not here to fulfil my dreams of being a games designer (I hate design. I always live in a dirt hole), I'm just here to code something we should have had by now.

HighlyLogical WILL ALWAYS keep the same feature set. Once I've released this, that's all there's going to be, in terms of content.
I'm too lazy for anything but update compatibility fixes after that, and even those I'm hoping to farm out to interested 3rd parties.
This is good news for you; it means you don't have to work out why the latest version of my mod makes minecraft use 500MB of extra memory,
compared to the last version. It means I don't break compatibility because I wanted to be creative,
or show off my talent as an artist/engineer/modeller.

I write clean, understandable code, with comments explaining my intentions. Come help, it'll be fun.

%tags:software,rant,ideas
