
How is it that all the innocence can be sucked out of a room in a single text,  
how can i plummet so far from a place so high, and yet not die from the dizzying vertigo?  

There was once a version of me who could raise an arm in action,  
who could make some attempt, feeble or not, to prevent his own demise  
(or even merely his slump into the turgid entropy of apathy and compliance),  
but not now. He is gone.

I traded in the very poetry i so feebly mimic now,  
for the gain in proactivity which has recently deserted me.  
I am small and unimportant, and no matter what i do to try and forget that,  
I cannot stop that fact overuling my life, and willigness to live it.  

All feeling deserts me, all friends abandon me,  
or I them.  
All meaning crumbles away, silently forgotten and forever lost.  
Does anyone remember MySpace?  

Every mountain I am proud of climbing pales in comparison to the one visible from its summit,  
All my energy is spent standing still while others run past me, my former peers and fellow-passengers, no longer aware of me, until I have no more energy left, and i regress back to some pathetic and helpless man-child.  
I wonder if I am alone in my slopping, grinding self-imposed rut, and am neither sure whether I am comforted or horrified if I weren't.  
The embarassment of my accomplishments bears down on me like an imaginary crowd of judging colleagues, now long gone.

Why is it that it's only when i feel like this, that I become overwhelmingly sentimental for a magical past that never really existed?

From past experience of being myself, I know that I get like this,
and I also know that none of the times I am fantasising back to were really that much good;  
I know this as a fact, but I can't help feeling that it was all so much better before (before when?),
and want to wrap myself up in the warm, cuddly, edited blanket of the past.  
Where every other day my (now ex) best friends are round, it's summer,
and I only stop laughing or talking about culture to breathe or eat.
It never really happened. Or did it?

%tags: depression, navel gazing
