1. The principal use of customisation is to keep the interface the same between versions
2. After some point, anything but security or compatibility patches must be either feature creep, or poor original design
3. The probability of a piece of software doing any or all of the following after its 5th year, approaches 1 as a function of time:
	* sets up a foundation
	* writes its own unique license
	* creates a [new, universal documentation standard](https://xkcd.com/927/)
	* develops its own compilation software, from scratch
	* reinvents itself with a fresh, hip new look (read: massive feature cutdown), alienating its loyal user base, whilst failing to be noticed by new users, who all just use mobile now anyway

%tags: software, rant, thoughts
