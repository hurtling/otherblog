web development makes me want to quit computing altogether, because it's circular, illogical, contradictory,
mean-spirited, political, inconsistent, sly, often meaningless, incompetent, arrogant 
(never before has such a poorly-written mishmash of half-remembered, half-implemented differing committeefied 
standards of document markup acted so important to humankind), jealous, corporate, uncooperative, 
ugly (whilst pretending to be beautiful), inefficient and backwards.

How can this be good practice, let alone part of the most widely-used javascript library?
``` javascript
$(document).ready(function()
			{
            
            });
````
or this anonymous, immediately-called-once-only monstrostity?

this line.

``` javascript
data = data.replace(/divabc/g,"div");
```

this line shows that tumblr is deliberately breaking browsers that don't load their dishonest javascript-based deobfuscators.

It's wrong. Blackmail. Your browser WOULD support this page, but we don't like not having total control over how you see this,
so we've stopped you seeing ANYTHING AT ALL without enabling javascript! cool man!
(that last past was sarcastic, because they are a bunch of elitist mac bourgoise dickarses. Or possiby arsedicks.)
Or maybe this is what AJAX is. Wilfully ignoring standards in order to have more (brittle) trendycool pages that only work in Safari,
or whatever the rich kids think is cool these days.

`(function(arguments){some stuff})(args);`

just omit the bloody wrapping and just put your statements (The function body. The {} bit!) at ground level. Go on, it won't hurt.
How can you make it even uglier? I KNOW! PUT THE ANONYMOUS FUNCTION INSIDE A JSON DATA BLOCK!
Close bracket, because I CLOSE MY BRACKETS)
It's an insult to both the logical and creative sides of my brain, because it does both badly.

%tags: javascript, software, rant
